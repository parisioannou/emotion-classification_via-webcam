#### Emotion Classification using facial expressions - A Deep Learning project


I have trained a model to recognise 7 categories of emotion:

1. Angry
2. Disgust
3. Fear
4. Happy
5. Sad
5. Surprise
6. Neutral

Accuracy achieved: 63%

Dataset: FER2013, 
It consists of 34.034 (48x48 pixel) grayscale images of faces.
Link: https://bit.ly/3nZVjFr

The model has been deployed in a simpe Python app. This app uses the default camera(1) to first, detect faces(2) in the frame, and then by using facial expressions, it recognises the corresponding emotion. 
Each prediction is being displayed on the screen by text, as well as a different color -> According to the color theory, each color invokes an emotion. 

Frameworks/Libraries used: TendorFlow, Numpy, Pandas, OpenCV, Keras

(1)OpenCV is used to provide the camera function

(2)To detect faces I have implemented the default Haar Cascades model (https://bit.ly/3bMRXTQ)



